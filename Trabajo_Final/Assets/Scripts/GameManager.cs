using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class GameManager : MonoBehaviour
{

    public GameObject[] bot;


    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("SpawnearAhora", 5, Random.Range(2,5));
        InvokeRepeating("SpawnearAhora", 5, Random.Range(2, 5));
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetKeyDown(KeyCode.R))
        {
            SceneManager.LoadScene("proyecto");
        }
    }

    Vector3 posicionRandom()
    {
        float _x = Random.Range(-40, 40);
        float _y = 1f;
        float _z = Random.Range(-45, 45);

        Vector3 newPos = new Vector3(-_x, _y, _z);
        return newPos;
    }

    void SpawnearAhora()
    {
        Instantiate(bot[Random.Range(0, 2)], posicionRandom(), Quaternion.identity);

    }

    public void Pausar()
    {
        Time.timeScale = 0;
    }

    public void Reanudar()
    {
        Time.timeScale = 1;
    }
}
