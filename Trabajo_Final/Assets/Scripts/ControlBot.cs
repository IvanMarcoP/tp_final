using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlBot : MonoBehaviour
{
    public ParticleSystem Explosion;
    private int hp;
    private GameObject Jugador;
    public int rapidez;

    // Start is called before the first frame update
    void Start()
    {
        hp = 100;
        Jugador = GameObject.Find("Jugador");
    }

    private void Update()
    {
        transform.LookAt(Jugador.transform);
        transform.Translate(rapidez * Vector3.forward * Time.deltaTime);
    }

    public void recibirDaņo()
    {
        hp = hp - 25; 
        if (hp <= 0)
        {
            desaparecer();
        }
    }

    private void desaparecer()
    {
        Instantiate(Explosion, transform.position, Quaternion.identity);
        Destroy(gameObject);
        GestorDeAudio.instancia.ReproducirSonido("sonidoMuerte");
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Bala"))
        {
            recibirDaņo();
        }
    }

}
